import { Component } from '@angular/core';

@Component({
  template: `
    <div class="text-center pt-5">
      <h3>Page not found</h3>
      <a routerLink="/home">Home</a>
    </div>
  `,
})
export class NotFoundComponent {}
